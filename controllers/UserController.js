const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujcg6ed/collections/";
const mLabAPIKey = "apiKey=E61WZ1PMqQky2UAZcKzNynMKzqK-TmN-";

function getUsersV1(req,res){
  console.log("GET /apitechu/v1/users");

  var result = {};
  var users = require('../seguridadUsuarios.json');

  if (req.query.$count == "true"){
    console.log("Count needed");
    result.count = users.length;
  }
  result.users = req.query.$top ?
    users.slice(0, req.query.$top) : users;

  res.send(result);

}

function createUserV1(req,res){
  console.log("POST /apitechu/v1/users");
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);
  var newUser = {
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email
  };

  var users = require('../seguridadUsuarios.json');
  users.push(newUser);

  io.writeUserDataToFile(users);
  res.send(result);

}

function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");
  var idFind = req.params.id;
  console.log("id es "+idFind);
  var users = require('../seguridadUsuarios.json');

  // SOLUCION 1
/*    for (user of users){
    console.log("Length of array is "+users.length);
    if (user != null && user.id == idFind){
      console.log("La ID coincide");
      //delete users[user.id - 1];
      users.splice(user.id - 1, 1);
      break;
    }
  }
*/    // SOLUCION 2
  for (arrayID in users) {
      var id_X = users[arrayID].id;
      var name_X = users[arrayID].first_name;
      if (id_X == idFind){
        console.log("Encontrado el nombre del id "+id_X+" es "+name_X);
        // Con splice
        //users.splice(arrayID, 1);
        // Con delete
        delete users[arrayID];
        break;
      }
  }

  // SOLUCION 3
/*    users.forEach(function(user,index){
    if (user.id==idFind){
      console.log("La ID coincide");
      users.splice(index, 1);
    }
  })
*/
  console.log("Usuario con id "+id_X+" borrado con exito");
  io.writeUserDataToFile(users);
  res.send({"msg" : "Usuario con id "+id_X+" borrado con exito" });
}

function getUsersV2(req,res){
  console.log("GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  );
}

function getUserByIdV2(req,res){
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id" : ' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
            var response = {
              "msg" : "Error obteniendo usuarios"
            }
            res.status(500);
        }else{
          if (body.length > 0){
            var response = body[0];
          }else{
              var response = {
              "msg" : "Usuario no encontrado"
              }
            res.status(404);
          }
        }
        res.send(response);
      }
  );
}

function createUserV2(req,res){
  console.log("POST /apitechu/v2/users");
  console.log("ID es " + req.body.id);
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);

  var newUser = {
    "id" : req.body.id,
    "first_name" : req.body.first_name,
    "last_name" : req.body.last_name,
    "email" : req.body.email,
    "password" : crypt.hash(req.body.password)
  };

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body){
      console.log("Usuario creado con exito");
      res.send({"msg" : "Usuario creado con exito"})
    }
  )
}

module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
