const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujcg6ed/collections/";
const mLabAPIKey = "apiKey=E61WZ1PMqQky2UAZcKzNynMKzqK-TmN-";

function loginV1(req,res){
  console.log("POST apitechu/v1/login/");
  //console.log("Body");
  var usuarioLoginEmail = req.body.email;
  //console.log("Usuario = "+usuarioLoginEmail);
  var usuarioPwdEmail = req.body.password;
  //console.log("Password = "+usuarioPwdEmail);

  var users = require('../seguridadUsuarios.json');
  var result = {};

  var encontrado = false;
  for (user of users){
    //console.log("Usuario : "+user.email);
    if (user != null && user.email == usuarioLoginEmail){
      //console.log("Usuario Encontrado");
      encontrado = true;
      if (user.password == usuarioPwdEmail){
        //console.log("Password SÍ coincide");
        user.logged = true;
        result.idUsuario = user.id;
        result.mensaje = "LOGIN correcto";
      }else{
        //console.log("Password NO coincide");
        result.mensaje = "LOGIN incorrecto -> Password NO coincide";
      }
      break;
    }
  }
  if (!encontrado){
    //console.log("Usuario NO Encontrado");
    result.mensaje = "LOGIN incorrecto -> Usuario NO Encontrado";
  }

  //console.log("Finalizado");
  io.writeUserDataToFile(users);
  res.send(result);
}

function logoutV1(req,res){
  console.log("POST apitechu/v1/logout/");
  //console.log("Body");
  var usuarioLogoutId = req.body.id;
  console.log("Usuario ID = "+usuarioLogoutId);

  var users = require('../seguridadUsuarios.json');
  var result = {};

  var encontrado = false;
  for (user of users){
    //console.log("Usuario : "+user.email);
    if (user != null && user.id == usuarioLogoutId){
      //console.log("Usuario Encontrado");
      encontrado = true;
      if (user.logged){
        //console.log("Usuario estaba conectado");
        delete user.logged;
        result.idUsuario = user.id;
        result.mensaje = "LOGOUT correcto";
      }else{
        //console.log("Usuario NO estaba conectado");
        result.mensaje = "LOGOUT incorrecto -> Usuario NO estaba conectado";
      }
      break;
    }
  }
  if (!encontrado){
    //console.log("Usuario NO Encontrado");
    result.mensaje = "LOGOUT incorrecto -> Usuario NO Encontrado";
  }

  //console.log("Finalizado");
  io.writeUserDataToFile(users);
  res.send(result);
}

function loginV2(req,res){
  console.log("POST /apitechu/v2/login");
  var usuarioLoginEmail = req.body.email;
  var usuarioLoginPwd = req.body.password;

  var query = 'q={"email" : "' + usuarioLoginEmail + '"}';
  console.log("query es "+query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("httpClient created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
        if (err){
          console.log("Error realizando login");
          response = {
              "msg" : "Error realizando login"
            }
            res.status(500);
            res.send(response);

        }else{
          if (body.length > 0){
            console.log("Usuario encontrado");
            var usuarioMongoPwd = body[0].password;
            var isPwdCorrect = crypt.checkPassword(usuarioLoginPwd,usuarioMongoPwd);
            if (isPwdCorrect){
              console.log("Usuario y password coinciden");
              query = 'q={"id" : ' + body[0].id +'}';
              console.log("Query for put is " + query);
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT){
                  console.log("LOGIN correcto");
                  var response = {
                    "msg" : "LOGIN correcto",
                    "idUsuario" : body[0].id
                  }
                  res.send(response);
                }
              );
            }else{
              console.log("Usuario y password no coinciden");
              var response = {
              "msg" : "Usuario y password no coinciden"
              }
              //res.status(404);
              res.send(response);
            }
          }else{
              console.log("Usuario no encontrado");
              var response = {
              "msg" : "Usuario no encontrado"
              }
              //res.status(404);
              res.send(response);
          }
        }
      }
  );

}

function logoutV2(req,res){
  console.log("POST /apitechu/v2/logout/:id");
  var usuarioIDLogout = req.params.id;
  console.log("id logout : "+usuarioIDLogout);

  var query = 'q={"id" : "' + usuarioIDLogout + '"}';
  console.log("query es "+query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("httpClient created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if (err){
        console.log("Error realizando logout");
        response = {
            "msg" : "Error realizando logout"
        }
        //res.status(500);
        res.send(response);

      }else{
        if (body.length > 0){
          console.log("Usuario encontrado");
          var putBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT){
              console.log("LOGOUT correcto");
              response = {
                "msg" : "LOGOUT correcto",
                "idUsuario" : body[0].id
              }
              res.send(response);
            }
          );
        }else{
            console.log("LOGOUT incorrecto");
            response = {
            "msg" : "LOGOUT incorrecto"
            }
          //res.status(404);
          res.send(response);

        }
      }
    }
  );
}

module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
