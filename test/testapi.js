const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
var server = require('../server');

describe('First unit test',
  function(){
      it('Test that Duckduckgo works',function(done){
        chai.request('http://www.duckduckgo.com')
        //chai.request('https://developer.mozilla.org/en-US/asfasdfas')
          .get('/')
          .end(
            function(err,res){
              console.log("Request has finished");
              //console.log(err);
              //console.log(res);
              res.should.have.status(200);
              done();
            }
          )
      }
    )
  }
)

describe('Test de API de usuarios',
  function(){
      it('Prueba de que la API de usuarios responde',function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/hello')
          .end(
            function(err,res){
              console.log("Request has finished");
              res.should.have.status(200);
              done();
            }
          )
        }
      ),
      it('Prueba de que la API devuelve una lista de usuarios correcta',function(done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
            function(err,res){
              console.log("Request has finished");
              res.should.have.status(200);
              res.body.users.should.be.a("array");
              for (user of res.body.users){
                user.should.have.property('email');
                user.should.have.property('first_name');
                
              }
              done();
            }
          )
        }
      )

  }
)
